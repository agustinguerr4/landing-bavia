


// Smooth Scroll

$(document).ready(function () {
  $('a[href^="#"]').click(function () {
    // 1
    e.preventDefault();
    // 2
    const href = $(this).attr("href");
    // 3
    $("html, body").animate({ scrollTop: $(href).offset().top }, 800);
  });
});



// Slider Home 

document.addEventListener('DOMContentLoaded', function () {
  var splide = new Splide('.splide', {
    autoplay: 'playing',
    rewind: true,
    interval: 3000,
    type: 'fade'
  });
  splide.on('autoplay:playing', function (rate) {
    
  });
  splide.mount();
});

$(document).ready(function () {
  $(".splide__arrow--next")[0].innerHTML = `<img src="/img/arrow-right.png" alt="Next">`
  $(".splide__arrow--prev")[0].innerHTML = `<img src="/img/arrow-left.png" alt="Prev">`
})


$(document).ready(function () {
  toggleMenu = function () {
    if ($(window).width() < 961) {
      if ($(".mobile-menu").hasClass('hide')) {
        $(".mobile-menu").removeClass('hide');
        $(".mobile-menu").addClass('show');
       } else {
        $(".mobile-menu").addClass("hide");
        }
    }
  }

})


$(document).ready(function(){
  $(".link-mobile-menu").on('click',function () {
    $(".mobile-menu").addClass("hide"); 
    })
})

//Loader
$(window).on('load',function() {
  $('#loading').hide();
});

$(window).scroll(function (event) {
  var scroll = $(window).scrollTop();
  (scroll > 100) 
  ? 
  $("#back-to-top").show()
  : 
  
  $("#back-to-top").hide()
});

